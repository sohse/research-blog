from __future__ import division
import sys
sys.path.insert(0, '/home/sohse/projects/PUBLICATION/annotation')
import re
from collections import defaultdict
from itertools import combinations
import random
import datetime

import scipy as sp
import h5py
import seaborn as sb
import pylab as pl

from database import Annotation

sp.random.seed(42)




def clean(file_names):
    '''
    Cleans up a list of file names into proper gsms.
    '''
    cleaned  = [re.split(r'[_|.]', file_name)[0] for file_name in file_names]
    return cleaned




def histograms(within_gse=False):

    a = Annotation('/home/sohse/projects/TEMP/Hauke/GEOmetadb.sqlite')

    for gpl, animalTFDB in zip(['GPL1261', 'GPL96', 'GPL570'], ['Mus_musculus_TF_EnsemblID', 'Homo_sapiens_TF_EnsemblID', 'Homo_sapiens_TF_EnsemblID']):
        print gpl

        # Load SCAN normalized dataset

        with h5py.File('/home/sohse/projects/NCBI/Normalization_SCAN_{gpl}.h5'.format(gpl=gpl), 'r') as f:
            temp = sp.asarray(clean(sp.asarray(f['features'])))
            subset = [index for (index, feature) in enumerate(temp) if 'AFFX' not in feature]
            features = list(temp[subset])
            data = sp.asarray(f['SCAN'])[:, subset]
            samples = clean(sp.asarray(f['samples']))

        sample__index = dict(zip(samples, range(len(samples))))
        feature__index = dict(zip(features, range(len(features))))

        absolute_fold_changes = []

        if within_gse:

            gse__gsms = defaultdict(list)

            for record in a.query('gse_gsm', rows=samples):
                gse__gsms[record['gse']].append(sample__index[record['gsm']])

            # Randomly select 500 gse sets and evaluate all contained contrasts (with max. size 100).

            gses = gse__gsms.keys()
            random.shuffle(gses)

            for n, gse in enumerate(gses[:500]):
                #print n, len(gse__gsms[gse])
                if len(gse__gsms[gse]) > 100:
                    continue
                for i, j in combinations(gse__gsms[gse], 2):
                    absolute_fold_changes.append(sp.absolute(data[i] - data[j]))
        else:
            sample_indices = range(len(samples))

            # Poor man's version because combinations(sample_indices, 2) would take too long.

            sp.random.shuffle(sample_indices)
            len_ = len(sample_indices)//2
            a = sample_indices[:len_]
            b = sample_indices[len_:2*len_]
            for i, j in zip(a, b):
                absolute_fold_changes.append(sp.absolute(data[i] - data[j]))

        absolute_fold_changes = sp.vstack(absolute_fold_changes)
        print 'Total number of sample contrasts for {gpl}:'.format(gpl=gpl), len(absolute_fold_changes)

        if within_gse:
            sp.save('{gpl}_absolute_fold_changes_{time}_within_gse'.format(gpl=gpl, time=time), absolute_fold_changes)
        else:
            sp.save('{gpl}_absolute_fold_changes_{time}'.format(gpl=gpl, time=time), absolute_fold_changes)

        transcription_factors = sp.genfromtxt('/home/sohse/projects/PUBLICATION/analysis/' + animalTFDB + '.txt', dtype=str)
        transcription_factors_indices = sorted(set([feature for feature in map(feature__index.get, transcription_factors) if feature is not None]))

        print 'Number of transcription factors for {gpl}:'.format(gpl=gpl), len(transcription_factors_indices)

        remainders_indices = sorted(set(range(len(features))) - set(transcription_factors_indices))
        random.shuffle(remainders_indices)
        remainders_indices = remainders_indices[:len(transcription_factors_indices)]
        assert len(remainders_indices) == len(transcription_factors_indices)

        fig = pl.figure(figsize=(20, 10))
        ax = fig.add_subplot(111)
        weights_remainders = sp.ones_like(absolute_fold_changes[:, remainders_indices].ravel())/len(remainders_indices)
        weights_transcription_factors = sp.ones_like(absolute_fold_changes[:, transcription_factors_indices].ravel())/len(transcription_factors_indices)
        ax.hist(absolute_fold_changes[:, remainders_indices].ravel(), weights=weights_remainders, bins=100, color='grey', alpha=0.6, histtype='stepfilled', label='Remaining genes')
        ax.hist(absolute_fold_changes[:, transcription_factors_indices].ravel(), weights=weights_transcription_factors, bins=100, color='blue', alpha=0.4, histtype='stepfilled', label='Transcription factors')
        ax.set_yscale('log', nonposy='clip')
        ax.set_ylabel('Number of contrasts (log) | within GSEs only')
        ax.set_xlabel('Fold change (log)')
        ax.set_xlim(-0.5, 8)
        ax.set_ylim(0.0, 1.0e5)
        pl.legend(loc='best')
        if within_gse:
            ax.set_title(gpl) #  + ' | Total number of contrasts: ' + str(len(absolute_fold_changes)) + ' | within GSEs only'
            fig.savefig('{gpl}_hist_within_gse'.format(gpl=gpl))
        else:
            ax.set_title(gpl) #  + ' | Total number of contrasts: ' + str(len(absolute_fold_changes))
            fig.savefig('{gpl}_hist'.format(gpl=gpl))




def iqrs_fold(within_gse=False):

    for gpl in ['GPL1261', 'GPL96', 'GPL570']:
        print gpl

        with h5py.File('/home/sohse/projects/NCBI/Normalization_SCAN_{gpl}.h5'.format(gpl=gpl), 'r') as f:
            temp = sp.asarray(clean(sp.asarray(f['features'])))
            subset = [index for (index, feature) in enumerate(temp) if 'AFFX' not in feature]
            features = list(temp[subset])

        if within_gse:
            file_name = 'iqr_{gpl}_{time}_within_gse.csv'.format(gpl=gpl, time=time)
            absolute_fold_changes = sp.load('{gpl}_absolute_fold_changes_{time}_within_gse.npy'.format(gpl=gpl, time=time))
        else:
            file_name = 'iqr_{gpl}_{time}.csv'.format(gpl=gpl, time=time)
            absolute_fold_changes = sp.load('{gpl}_absolute_fold_changes_{time}.npy'.format(gpl=gpl, time=time))

        with open(file_name, 'wb') as f:
            for feature, row in zip(features, absolute_fold_changes.T):
                q_75, q_25 = sp.percentile(row, [75 ,25])
                iqr = q_75 - q_25
                f.write(feature + ', ' + str(iqr) + '\n')




def iqrs_absolute():

    for gpl in ['GPL1261', 'GPL96', 'GPL570']:
        print gpl

        with h5py.File('/home/sohse/projects/NCBI/Normalization_SCAN_{gpl}.h5'.format(gpl=gpl), 'r') as f:
            temp = sp.asarray(clean(sp.asarray(f['features'])))
            subset = [index for (index, feature) in enumerate(temp) if 'AFFX' not in feature]
            features = list(temp[subset])
            data = sp.asarray(f['SCAN'])[:, subset]

        with open('absolute_expression_iqr_{gpl}_{time}.csv'.format(gpl=gpl, time=time), 'wb') as f:
            for feature, row in zip(features, data.T):
                q_75, q_25 = sp.percentile(row, [75 ,25])
                iqr = q_75 - q_25
                f.write(feature + ', ' + str(iqr) + '\n')




def correlation(within_gse=False):

    for gpl, animalTFDB in zip(['GPL1261', 'GPL96', 'GPL570'], ['Mus_musculus_TF_EnsemblID', 'Homo_sapiens_TF_EnsemblID', 'Homo_sapiens_TF_EnsemblID']):
        print gpl

        if within_gse:
            a = sp.genfromtxt('absolute_expression_iqr_{gpl}_{time}.csv'.format(gpl=gpl, time=time), usecols=[1])
            b = sp.genfromtxt('iqr_{gpl}_{time}_within_gse.csv'.format(gpl=gpl, time=time), usecols=[1])

            with h5py.File('/home/sohse/projects/NCBI/Normalization_SCAN_{gpl}.h5'.format(gpl=gpl), 'r') as f:
                temp = sp.asarray(clean(sp.asarray(f['features'])))
                subset = [index for (index, feature) in enumerate(temp) if 'AFFX' not in feature]
                features = list(temp[subset])

            feature__index = dict(zip(features, range(len(features))))
            transcription_factors = sp.genfromtxt('/home/sohse/projects/PUBLICATION/analysis/' + animalTFDB + '.txt', dtype=str)
            transcription_factors_indices = sorted(set([feature for feature in map(feature__index.get, transcription_factors) if feature is not None]))
            remainders_indices = sorted(set(range(len(features))) - set(transcription_factors_indices))

            fig = pl.figure(figsize=(10, 10))
            ax = fig.add_subplot(111)
            ax.scatter(a[remainders_indices], b[remainders_indices], marker='.', color='grey', alpha=0.6, label='Remaining genes')
            ax.scatter(a[transcription_factors_indices], b[transcription_factors_indices], marker='.', color='blue', alpha=0.4, label='Transcription factors')
            ax.set_xlim(0.0, 4.0)
            ax.set_ylim(0.0, 4.0)
            ax.plot(ax.get_xlim(), ax.get_ylim(), ls='--', c='0.3')
            ax.set_xlabel('Absolute expression IQR')
            ax.set_ylabel('Fold change IQR | within GSEs only')
            ax.set_title(gpl)
            pl.legend(loc='best')
            fig.savefig('{gpl}_scatter_within_gse'.format(gpl=gpl))
        else:
            a = sp.genfromtxt('absolute_expression_iqr_{gpl}_{time}.csv'.format(gpl=gpl, time=time), usecols=[1])
            b = sp.genfromtxt('iqr_{gpl}_{time}.csv'.format(gpl=gpl, time=time), usecols=[1])

            with h5py.File('/home/sohse/projects/NCBI/Normalization_SCAN_{gpl}.h5'.format(gpl=gpl), 'r') as f:
                temp = sp.asarray(clean(sp.asarray(f['features'])))
                subset = [index for (index, feature) in enumerate(temp) if 'AFFX' not in feature]
                features = list(temp[subset])

            feature__index = dict(zip(features, range(len(features))))
            transcription_factors = sp.genfromtxt('/home/sohse/projects/PUBLICATION/analysis/' + animalTFDB + '.txt', dtype=str)
            transcription_factors_indices = sorted(set([feature for feature in map(feature__index.get, transcription_factors) if feature is not None]))
            remainders_indices = sorted(set(range(len(features))) - set(transcription_factors_indices))

            fig = pl.figure(figsize=(10, 10))
            ax = fig.add_subplot(111)
            ax.scatter(a[remainders_indices], b[remainders_indices], marker='.', color='grey', alpha=0.6, label='Remaining genes')
            ax.scatter(a[transcription_factors_indices], b[transcription_factors_indices], marker='.', color='blue', alpha=0.4, label='Transcription factors')
            ax.set_xlim(0.0, 4.0)
            ax.set_ylim(0.0, 4.0)
            ax.plot(ax.get_xlim(), ax.get_ylim(), ls='--', c='0.3')
            ax.set_xlabel('Absolute expression IQR')
            ax.set_ylabel('Fold change IQR')
            ax.set_title(gpl)
            pl.legend(loc='best')
            fig.savefig('{gpl}_scatter'.format(gpl=gpl))


if __name__ == '__main__':

    time = datetime.datetime.now().strftime('%Y-%m-%d')

    print 'random'
    histograms()
    iqrs_fold()
    print 'within'
    histograms(within_gse=True)
    iqrs_fold(within_gse=True)
    print 'absolute'
    iqrs_absolute()
    print 'correlation'
    correlation()
    correlation(within_gse=True)







        